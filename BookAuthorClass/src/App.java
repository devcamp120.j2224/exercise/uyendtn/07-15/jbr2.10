public class App {
    public static void main(String[] args) throws Exception {
        Author author1 = new Author("Nguyễn Nhật Ánh", "anhnn@gmail.com", 'm');
        Author author2 = new Author("Ploy","ploy@gmail.com",'f');

        System.out.println(author1.toString());
        System.out.println(author2.toString());

        Book book1 = new Book("Kính vạn hoa", author1, 50000);
        Book book2 = new Book("Dạ khúc", author2, 150000,50);
        System.out.println(book1.toString());
        System.out.println(book2.toString());
    }
}
